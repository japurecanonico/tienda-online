import { XMarkIcon } from "@heroicons/react/24/outline";
import { useContext } from "react";
import { CarritoContext } from "../../Context";
import "./styles.css";

const ProductDetail = () => {
  const context = useContext(CarritoContext);

  return (
    <aside
      className={`${
        context.showDetails ? "left-0" : "-left-full"
      } transition-all w-full md:w-[40%] lg:w-[24%] h-[85vh] lg:h-[90vh] flex flex-col fixed bottom-0 shadow-md bg-white rounded-tr-lg p-3 z-50`}
    >
      <div className="flex justify-between">
        <h2 className="font-bold text-lg">Detail</h2>
        <button>
          <XMarkIcon
            className="h-6 w-6 text-orange-400"
            onClick={context.closeDetails}
          />
        </button>
      </div>

      <figure className="my-6 h-1/3">
        <img
          src={context.dataProduct?.image}
          alt={context.dataProduct?.name}
          className="h-full w-full object-contain"
        />
      </figure>

      <div className="flex flex-col h-[60%] overflow-y-auto pr-2">
        <span className="text-2xl">$ {context.dataProduct?.price}</span>
        <span className="font-medium my-2">{context.dataProduct?.title}</span>
        {/* <p className="text-sm">
          <span className="bg-orange-400 text-white rounded-lg px-2 py-1">
            {context.dataProduct?.category}
          </span>
        </p> */}
        <span className="text-sm">{context.dataProduct?.description}</span>
      </div>
    </aside>
  );
};

export default ProductDetail;
