import CheckoutSideMenu from "../../Components/CheckoutSideMenu";


const Layout = ({ children }) => {
  return (
    <div className="flex flex-col items-center py-8 relative">
      {children}
      <CheckoutSideMenu />
    </div>
  );
};

export default Layout;
