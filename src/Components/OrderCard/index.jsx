import { XMarkIcon } from "@heroicons/react/24/outline";
import { useContext } from "react";
import { CarritoContext } from "../../Context";

const OrderCard = (props) => {
  const { id, title, image, price } = props.item;
  const context = useContext(CarritoContext);

  let renderXmarkIcon;
  if (props.enableDelete) {
    renderXmarkIcon = (
      <XMarkIcon
        onClick={() => context.removeProductCart(id)}
        className="h-6 w-6 text-orange-400 cursor-pointer"
      ></XMarkIcon>
    );
  }

  return (
    <div
      key={id}
      className="flex justify-between items-center my-4 pb-2 border-b border-gray-300"
    >
      <div className="flex items-center gap-2">
        <figure className="w-10 h-10">
          <img
            src={image}
            alt={title}
            className="h-full w-full object-contain"
          />
        </figure>
        <span className="text-sm">{title}</span>
      </div>
      <div className="flex items-center gap-2">
        <span className="font-medium whitespace-nowrap pl-1">${price}</span>
        {renderXmarkIcon}
      </div>
    </div>
  );
};

export default OrderCard;
