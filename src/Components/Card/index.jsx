import { PlusIcon, CheckIcon } from "@heroicons/react/24/outline";
import { useContext } from "react";
import { CarritoContext } from "../../Context";

const Card = (item) => {
  const context = useContext(CarritoContext);

  const showProduct = (item) => {
    context.openDetails();
    context.setDataProduct(item);
  };

  const addToCart = (item) => {
    context.addProductCart(item);
  };

  const renderIcon = (id) => {
    // Verificar si el producto ya esta en el carrito
    const isInCart =
      context.cartProducts.filter((product) => product.id === id).length > 0;

    if (isInCart) {
      return (
        <div className="absolute top-0 right-0 flex justify-center item-center bg-gray-300 rounded-full p-1 m-2">
          <CheckIcon className="text-white w-4 h-4" />
        </div>
      );
    } else {
      return (
        <div
          className="absolute top-0 right-0 flex justify-center item-center bg-orange-400 rounded-full p-1 m-2"
          onClick={() => addToCart(item.item)}
        >
          <PlusIcon className="text-white w-4 h-4" />
        </div>
      );
    }
  };

  return (
    <div className="bg-white p-4 cursor-pointer h-64 text-black rounded-lg relative transition-all shadow-sm hover:shadow-md hover:scale-110 hover:z-10">
      <figure className="relative mb-2 w-full h-3/4">
        <img
          src={item.item.image}
          alt={item.item.title}
          className="w-full h-full object-contain"
          onClick={() => showProduct(item.item)}
        />
        <span className="absolute bottom-0 left-0 bg-white/60 rounded-lg text-xs m-2 px-3 py-0.5">
          {item.item.category}
        </span>
      </figure>
      {renderIcon(item.item.id)}
      <div
        className="flex flex-col lg:flex-row lg:items-center justify-between space-x-3 text-center"
        onClick={() => showProduct(item.item)}
      >
        <span className="text-sm font-light text-ellipsis whitespace-nowrap overflow-hidden">
          {item.item.title}
        </span>
        <span className="font-medium lg:text-right whitespace-nowrap">
          ${item.item.price}
        </span>
      </div>
    </div>
  );
};

export default Card;
