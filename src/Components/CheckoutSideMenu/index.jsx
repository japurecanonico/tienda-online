import { useContext } from "react";
import OrderCard from "../OrderCard";
import { CarritoContext } from "../../Context";
import { totalPrice } from "../../utils";
import { Link } from "react-router-dom";

const CheckoutSideMenu = () => {
  const context = useContext(CarritoContext);

  const orderRequest = () => {
    let newOrder = {
      id: Math.floor(Math.random() * 100),
      date: "01.02.23",
      products: context.cartProducts,
      totalProducts: context.cartProducts.length,
      totalPrice: totalPrice(context.cartProducts),
    };

    context.setOrder([...context.order, newOrder]);
    context.clearProductsCart();
  };

  return (
    <aside
      className={`${
        context.showCart ? "right-0" : "-right-full"
      } transition-all w-full md:w-[40%] lg:w-[24%] h-[85vh] lg:h-[90vh] flex flex-col fixed bottom-0 shadow-md bg-white rounded-tl-lg p-3`}
    >
      <h2 className="font-bold text-lg">Cart</h2>

      <div className="overflow-auto pr-2">
        {context.cartProducts.map((item) => (
          <OrderCard item={item} key={item.id} enableDelete />
        ))}
      </div>

      {context.cartProducts.length > 0 ? (
        <div className="flex flex-col justify-between mt-auto">
          <p className="flex justify-between font-medium my-2">
            <span>Total</span>
            <span>${totalPrice(context.cartProducts)}</span>
          </p>

          <Link to="/my-order/last">
            <button
              onClick={() => orderRequest()}
              className="bg-orange-400 hover:bg-orange-500 transition-all text-white rounded-full w-full block mt-auto px-2 py-1"
            >
              Checkout
            </button>
          </Link>
        </div>
      ) : (
        <p className="text-center mt-6 text-gray-500 text-sm">
          The cart is empty <br />
          let's go shopping!
        </p>
      )}
    </aside>
  );
};

export default CheckoutSideMenu;
