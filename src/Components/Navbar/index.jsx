import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { ShoppingCartIcon } from "@heroicons/react/24/outline";
import { CarritoContext } from "../../Context";
import "./styles.css";

const Navbar = () => {
  const context = useContext(CarritoContext);
  const [categories, setCategories] = useState([]);
  const [loadingNav, setLoadingNav] = useState(true);

  useEffect(() => {
    fetch("https://fakestoreapi.com/products/categories")
      .then((res) => res.json())
      .then((json) => {
        setCategories(json);
        setLoadingNav(false);
      });
  }, []);

  return (
    <nav className="grid grid-cols-1 lg:grid-cols-2 text-sm font-light bg-black text-white">
      <ul className="flex justify-center lg:justify-start items-center space-x-4 px-6 py-3">
        <li className="text-orange-400 text-2xl font-bold">
          <NavLink to="/">JShop</NavLink>
        </li>
        {!loadingNav && (
          <>
            <li className="hidden sm:list-item link-underline">
              <NavLink
                to="/"
                onClick={() => context.setSearchCategory("")}
                className={({ isActive }) =>
                  isActive ? "text-orange-400 focus-underline" : "hover-underline"
                }
              >
                All
              </NavLink>
            </li>

            {categories.map((item, i) => (
              <li
                key={i}
                className="capitalize hidden sm:list-item whitespace-nowrap link-underline"
              >
                <NavLink
                  to={`/${item}`}
                  onClick={() => context.setSearchCategory(item)}
                  className={({ isActive }) =>
                    isActive ? "text-orange-400 focus-underline" : "hover-underline"
                  }
                >
                  {item}
                </NavLink>
              </li>
            ))}
          </>
        )}
      </ul>

      <ul className="flex flex-wrap justify-center lg:justify-end items-center space-x-4 bg-gray-900 lg:bg-black px-6 py-3">
        {/* <li className="text-gray-100">japurecanonico@email.com</li> */}
        <li>
          <NavLink
            to="/my-orders"
            className={({ isActive }) => (isActive ? "text-orange-400" : "")}
          >
            My Orders
          </NavLink>
        </li>
        {/* <li>
          <NavLink
            to="/my-account"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : ""
            }
          >
            My Account
          </NavLink>
        </li> */}
        {/* <li>
          <NavLink
            to="/signin"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : ""
            }
          >
            Signin
          </NavLink>
        </li> */}
        <li className="relative">
          <button onClick={() => context.setShowCart(!context.showCart)}>
            <ShoppingCartIcon className="h-6 w-6" />
            <span className="bg-orange-400 text-white h-5 w-5 absolute -top-2 -right-2 flex items-center justify-center rounded-full">
              {context.cartProducts.length}
            </span>
          </button>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
