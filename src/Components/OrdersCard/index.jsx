import {
  ShoppingBagIcon,
  BanknotesIcon,
  CalendarIcon,
  ChevronRightIcon
} from "@heroicons/react/24/outline";

const OrderCard = (props) => {
  const {id, totalPrice, totalProducts, date, products } = props.item;

  return (
    <div
    key={id} className="border-b border-gray-300">
      <p className="flex justify-between items-center mt-4">
        <span className="font-medium whitespace-nowrap">
          <ShoppingBagIcon className="h-6 w-6 inline-block mr-2" />
          {totalProducts} Items
        </span>
        <span className="font-medium whitespace-nowrap">
          <BanknotesIcon className="h-6 w-6 inline-block mr-2" />${totalPrice}
        </span>
        <span className="font-medium whitespace-nowrap">
          <CalendarIcon className="h-6 w-6 inline-block mr-2" />
          {date}
        </span>
        <span className="text-orange-400">
          <ChevronRightIcon  className="h-6 w-6 inline-block mr-2" />
        </span>
      </p>

      <div className="flex gap-3 my-2">
        {products.map((item) => (
          <figure className="w-10 h-10 rounded-full border border-gray-300 p-1">
            <img
              src={item.image}
              className="w-full h-full object-contain"
              key={item.id}
            />
          </figure>
        ))}
      </div>
    </div>
  );
};

export default OrderCard;
