import { useContext } from "react";
import { CarritoContext } from "../../Context";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout";
import OrdersCard from "../../Components/OrdersCard";

function MyOrders() {
  const context = useContext(CarritoContext);

  return (
    <Layout>
      <div className="bg-white rounded-lg p-4 w-[90%] lg:w-1/2 shadow-md">
        <h2 className="text-center font-bold mb-4 flex-1">My Orders</h2>

        {context.order.length > 0 ? (
          <div className="h-[60vh] overflow-auto pr-2">
            {context.order?.map((item, index) => (
              <Link key={index} to={`/my-order/${item.id}`}>
                <OrdersCard item={item} />
              </Link>
            ))}
          </div>
        ) : (
          <p className="text-center my-6 text-gray-500 text-sm">
            You don't have any orders, <br /> make your first purchase!
          </p>
        )}
      </div>
    </Layout>
  );
}

export default MyOrders;
