import Layout from "../../Components/Layout";
function MyAccount() {
  return (
    <div className="bg-pink-100">
      <Layout>My Account</Layout>
    </div>
  );
}

export default MyAccount;
