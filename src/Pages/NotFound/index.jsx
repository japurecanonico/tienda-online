function NotFound() {
  return <div className="bg-pink-100">
    Not Found
  </div>;
}

export default NotFound;
