import { useContext } from "react";
import Layout from "../../Components/Layout";
import Card from "../../Components/Card";
import ProductDetail from "../../Components/ProductDetail";
import { CarritoContext } from "../../Context";

function Home() {
  const context = useContext(CarritoContext);

  let renderProducts;
  if (context.loading) {
    renderProducts = (
      <p
        className="inline-block h-8 w-8 animate-spin rounded-full border-4 border-orange-400 border-r-transparent"
        role="status"
      ></p>
    );
  } else if (context.filteredProducts.length < 1) {
    renderProducts = (
      <p className="text-center my-6 text-gray-500 text-sm">
        Ups... <br /> The product you are looking for does not exist
      </p>
    );
  }

  return (
    <Layout>
      <h2 className="text-center font-bold">Exclusive Products</h2>

      <input
        type="text"
        placeholder="Search a product..."
        className="w-1/2 my-4 p-2 rounded-md text-center focus:outline-none shadow-inner"
        onChange={(e) => context.setSearchTitle(e.target.value)}
      />

      <div className="grid grid-cols-2 lg:grid-cols-3 gap-3 w-[90%] lg:w-1/2">
        {context.filteredProducts?.map((item) => (
          <Card key={item.id} item={item} />
        ))}
      </div>

      {renderProducts}
      <ProductDetail />
    </Layout>
  );
}

export default Home;
