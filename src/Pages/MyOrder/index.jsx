import { useContext } from "react";
import { CarritoContext } from "../../Context";
import { ChevronLeftIcon  } from "@heroicons/react/24/outline";
import { Link, useParams } from "react-router-dom";
import Layout from "../../Components/Layout";
import OrderCard from "../../Components/OrderCard";

function MyOrder() {
  const context = useContext(CarritoContext);
  const params = useParams();

  var orderSelected;

  if (params.id == "last") {
    orderSelected = context.order.slice(-1)[0];
  } else {
    orderSelected = context.order.filter((item) => item.id == params.id)[0];
  }

  return (
    <Layout>
      <div className="bg-white rounded-lg p-4 w-[90%] lg:w-1/2 shadow-md">
        <div className="flex justify-between">
          <Link to="/my-orders">
            <ChevronLeftIcon  className="h-6 w-6 text-orange-400" />
          </Link>
          <h2 className="text-center font-bold mb-4 flex-1">My Order</h2>
        </div>

        <div className="h-[60vh] overflow-auto pr-2">
          {orderSelected?.products?.map((item) => (
            <OrderCard item={item} key={item.id} />
          ))}
        </div>

        <p className="flex justify-between font-medium mt-3">
          <span>Total</span>
          <span>${orderSelected?.totalPrice || 0}</span>
        </p>
      </div>
    </Layout>
  );
}

export default MyOrder;
